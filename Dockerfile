FROM registry.gitlab.com/s3conf/nginx

RUN rm -rf /app/static/* \
    && rm -rf /etc/nginx \
    && apt-get update \
    && apt-get -y --no-install-recommends install software-properties-common \
    && add-apt-repository ppa:certbot/certbot \
    && apt-get update \
    && apt-get -y --no-install-recommends install certbot \
    && apt-get autoremove -yqq \
    && apt-get clean \
    && rm -Rf /tmp/* /var/tmp/* /var/lib/apt/lists/*

COPY etc /etc
COPY letsencrypt.py /app/letsencrypt.py
COPY crontab /app/crontab

RUN chmod 750 /etc/service/nginx/run \
    && crontab < /app/crontab

CMD ["/sbin/my_init"]
