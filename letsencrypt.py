import os
import subprocess
import sys
import logging

from s3conf.s3conf import S3Conf
from s3conf.storages import strip_s3_path, prepare_path


logger = logging.getLogger(__name__)
logging.getLogger('botocore').setLevel(logging.WARNING)


class Config:
    def __init__(self, config_path=None, remote_path=None, sites=None, email=None):
        self.sites = sites or os.environ.get('LETSENCRYPT_SITES')
        if self.sites:
            self.sites = [site.strip() for site in self.sites.split(';')]
        self.config_path = config_path or os.environ.get('LETSENCRYPT_BASE_PATH') or '/etc/letsencrypt'
        self.remote_path = remote_path or os.environ.get('LETSENCRYPT_REMOTE_PATH')
        if not self.remote_path:
            bucket, _ = strip_s3_path(os.environ['S3CONF'])
            letsencrypt_path = 's3://{}/'.format(os.path.join(bucket, 'letsencrypt'))
            self.remote_path = letsencrypt_path
        self.email = email or os.environ.get('LETSENCRYPT_EMAIL')
        self.application_log = 'docker_s3conf_letsencrypt.log'
        self.logs_path = os.path.join(self.config_path, 'logs')
        self.web_root = '/app/static/'
        self.archive_path = 'archive'
        self.live_path = 'live'


def run_subprocess(command):
    process = subprocess.run(
        command,
        stdout=subprocess.PIPE,
        stderr=subprocess.PIPE,
    )
    stdout = str(process.stdout, 'utf-8')
    if stdout:
        logger.info(stdout)
    stderr = str(process.stderr, 'utf-8')
    if stderr:
        logger.info(stderr)
    if process.returncode != 0:
        raise RuntimeError('Error executing subprocess.')


class Letsencrypt:
    def __init__(self, config_path=None, remote_path=None, sites=None, email=None):
        self.conf = Config(config_path=config_path, remote_path=remote_path, sites=sites, email=email)
        self.config_logging()
        self._s3 = None

    @property
    def s3(self):
        if not self._s3:
            self._s3 = S3Conf()
        return self._s3

    def config_logging(self):
        log_file_path = os.path.join(self.conf.logs_path, self.conf.application_log)
        prepare_path(log_file_path)
        logging.basicConfig(
            filename=log_file_path,
            level=logging.INFO,
            format='[%(asctime)s] [%(levelname)s] [%(module)s] [%(name)s] %(message)s',
        )
        stderr_logger = logging.StreamHandler()
        stderr_logger.setFormatter(logging.Formatter(logging.BASIC_FORMAT))
        logging.getLogger().addHandler(stderr_logger)

    def resolve_symlink(self, file_path):
        if file_path.endswith('.pem'):
            folder, file_name = os.path.split(file_path)
            _, site = os.path.split(folder)

            archive_path = os.path.join(self.conf.config_path, self.conf.archive_path, site)
            archive_files = os.listdir(archive_path)
            current = max(int(''.join(c for c in name if c.isdigit())) for name in archive_files)

            file_name_begin, file_name_end = file_name.split('.')
            current_archive_file = f'{file_name_begin}{current}.{file_name_end}'
            current_path = os.path.join(archive_path, current_archive_file).replace(self.conf.config_path, '../..')

            run_subprocess(['rm', '-f', file_path])
            run_subprocess(['ln', '-s', current_path, file_path])

    def fix_symlinks(self):
        live_path = os.path.join(self.conf.config_path, self.conf.live_path)
        for sites in os.listdir(live_path):
            site_path = os.path.join(live_path, sites)
            for file_name in os.listdir(site_path):
                self.resolve_symlink(os.path.join(site_path, file_name))

    def download_certificates(self):
        self.s3.download(self.conf.remote_path, self.conf.config_path)

    def upload_certificates(self):
        self.s3.upload(self.conf.config_path, self.conf.remote_path)

    def create_certificates(self):
        sites_options = []
        for site in self.conf.sites:
            sites_options.append('-d')
            sites_options.append(site)
        run_subprocess(
            [
                'certbot',
                'certonly',
                '--webroot',
                '-w',
                self.conf.web_root,
                '--email',
                self.conf.email,
                '--agree-tos',
                '--keep-until-expiring',
                '--preferred-challenges',
                'http',
                '--non-interactive',
                '--config-dir',
                self.conf.config_path,
                '--logs-dir',
                self.conf.logs_path,
            ] + sites_options
        )

    def renew_certificates(self):
        self.fix_symlinks()
        run_subprocess([
            'certbot',
            'renew',
            '--config-dir',
            self.conf.config_path,
            '--logs-dir',
            self.conf.logs_path,
        ])


if __name__ == '__main__':
    lets = Letsencrypt()
    if len(sys.argv) > 1 and sys.argv[1] == 'create':
        lets.create_certificates()
    else:
        lets.download_certificates()
        lets.renew_certificates()
    lets.upload_certificates()
